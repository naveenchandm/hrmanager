const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CandidateSkillSchema = new Schema({
  name: { type: String },
  email: { type: String, required: true },
  communication: [
    { scale: Number, required: true, default: null },
    { comments: String },
  ],
  attire: [
    { scale: Number, required: true, default: null },
    { comments: String },
  ],
  criticalThinking: [
    { scale: Number, required: true, default: null },
    { comments: String },
  ],
  bodyLanguage: [
    { scale: Number, required: true, default: null },
    { comments: String },
  ],
  confidence: [
    { scale: Number, required: true, default: null },
    { comments: String },
  ],
  leadership: [
    { scale: Number, required: true, default: null },
    { comments: String },
  ],
  finalComments: { comments: String },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  feedbackStatus: { type: Boolean, default: false },
  updatedby: { type: String, required: true },
});

module.exports = mongoose.model("CandidateProfile", CandidateSkillSchema);
