const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const DesignationSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  role: { type: String },
  firmName: { type: String },
});

module.exports = mongoose.model("Designationinfo", DesignationSchema);
