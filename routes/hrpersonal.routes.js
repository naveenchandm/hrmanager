const router = require("express").Router();
let hrInfo = require("../model/hrpersonal.model");

router.route("/add").post((req, res) => {
  const { name, email, role, firmName } = req.body;

  const newHR = new hrinfo({ name, email, role, firmName });

  newHR
    .save()
    .then(() => res.json("New HR added!"))
    .catch((err) => res.status(400).json("Error: " + err));
});

module.exports = router;
