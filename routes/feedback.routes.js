const router = require("express").Router();
let hrInfo = require("../model/feedback.model");

router.route("/add").post((req, res) => {
  const {
    name,
    email,
    communication,
    attire,
    criticalThinking,
    bodyLanguage,
    confidence,
    leadership,
    finalComments,
    updatedAt,
    updatedby,
  } = req.body;

  const newhr = new hrinfo({
    name,
    email,
    communication,
    attire,
    criticalThinking,
    bodyLanguage,
    confidence,
    leadership,
    finalComments,
    updatedAt,
    updatedby,
  });

  newhr
    .save()
    .then(() => res.json(`added ${name} feedback!`))
    .catch((err) => res.status(400).json("Error: " + err));
});

module.exports = router;
